import Vue from './vue.js'

import { Navbar } from './components/navbar.js'
import { MainLayout } from './components/main-layout.js'
import { About } from './components/view/about.js'
import { Surveys } from './components/view/surveys.js'
import { Evaluation } from './components/view/evaluation.js'
import { Teams } from './components/view/team.js'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [{
    path: '/about',
    component: About,
    name: "About Us Page"
  }, {
    path: '/surveys',
    component: Surveys,
    name: "Surveys"
  }, {
    path: '/evaluation/:surveyId',
    component: Evaluation,
    name: "Evaluation"
  }, {
    path: '/teams',
    component: Teams,
    name: "Teams"
  }]
})

new Vue({
  el: '#app',
  components: {
    'navbar': Navbar
  },
  router,
  template: MainLayout
})
