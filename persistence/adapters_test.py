import os

from assertpy import assert_that

from persistence.adapters import FileSystemStorage


def test_filesystem_adapter_creates_folders_on_write_if_not_exists(fs):
    adapter = FileSystemStorage()

    adapter.write_record("test", "foo", "bar")

    assert_that("test").exists().is_directory()


def test_filesystem_adapter_writes_given_content_to_file(fs):
    fs.create_dir("test")
    adapter = FileSystemStorage()

    adapter.write_record("test", "test.txt", "some test content")

    assert_that(os.path.exists("test/test.txt")).is_true()
    with open("test/test.txt") as test_file:
        assert_that(test_file.read()).is_equal_to("some test content")


def test_filesystem_adapter_returns_ids_for_domain(fs):
    fs.create_file("/test/domain/a.json")
    fs.create_file("/test/domain/b.json")
    adapter = FileSystemStorage("test")

    result = adapter.get_record_ids_for("domain")

    assert_that(result).contains("a", "b").is_length(2)


def test_filesystem_adapter_returns_record_if_exists(fs):
    fs.create_file("/test/domain/foo", contents="bar")
    adapter = FileSystemStorage("test")

    result = adapter.get_record("domain", "foo")

    assert_that(result).is_equal_to("bar")
