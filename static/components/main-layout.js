const MainLayout = `
<div id="main-layout">
    <navbar></navbar>
    <div class="ui padded segment">
        <router-view></router-view>
    </div>
</div>
`
export { MainLayout }