from datetime import datetime
from unittest.mock import Mock

import pandas as pandas
from assertpy import assert_that, fail
from domain.survey import Survey, IllegalTeamNameError, SurveyFactory, IllegalSurveyIdentifier
from domain.survey_vobj import SourceURI


def test_survey_can_have_a_team():
    survey = Survey()
    survey.associated_team = "test team"
    assert_that(survey.associated_team).is_equal_to("test team")


def test_survey_team_name_must_not_be_empty():
    survey = Survey()
    try:
        survey.associated_team = " "
    except IllegalTeamNameError:
        pass
    else:
        fail("IllegalTeamName error not thrown when expected")


def test_survey_builder_automatically_assigns_creation_date_and_id():
    survey = SurveyFactory.new_survey()
    assert_that_survey_is_basically_valid(survey)


def test_survey_factory_creates_survey_from_valid_minimal_dict():
    mock_date = datetime.now()
    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(mock_date)})
    assert_that(survey.id).is_equal_to("test-id")
    assert_that(survey.creation_date).is_equal_to(mock_date)


def test_survey_can_set_and_read_valid_identifier():
    survey = SurveyFactory.new_survey()
    survey.identifier = "test-1"
    assert_that(survey.identifier).is_equal_to("test-1")


def test_survey_identifier_must_not_be_empty_string():
    survey = SurveyFactory.new_survey()
    try:
        survey.identifier = " "
    except IllegalSurveyIdentifier:
        pass
    else:
        fail("survey identifier must not be empty")


def test_survey_identifier_must_not_contain_spaces():
    survey = SurveyFactory.new_survey()
    try:
        survey.identifier = "a b c"
    except IllegalSurveyIdentifier:
        pass
    else:
        fail("survey identifier must not contain spaces")


def test_survey_factory_can_create_survey_with_identifier():
    survey = SurveyFactory.with_identifier("test-1-2-3")
    assert_that(survey.identifier).is_equal_to("test-1-2-3")
    assert_that_survey_is_basically_valid(survey)


def test_survey_factory_restores_identifier_from_dict():
    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(datetime.now()),
                                      "identifier": "test-1-2-3"})
    assert_that(survey.identifier).is_equal_to("test-1-2-3")


def test_source_uri_with_same_uri_is_equal():
    uri_a = SourceURI("a:b")
    uri_b = SourceURI("a:b")

    assert_that(uri_a).is_equal_to(uri_b)


def test_source_uri_with_different_uris_are_not_equal():
    uri_a = SourceURI("a:b")
    uri_b = SourceURI("b:a")

    assert_that(uri_a).is_not_equal_to(uri_b)


def test_can_add_source_to_a_survey():
    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(datetime.now())})
    survey.add_source(SourceURI("gspread:1U2aVfh8i-wwy4hV_b18T6AeIwetKSHao_dNPxjwKudc/edit#gid=935903276"))
    assert_that(survey.sources).contains(SourceURI("gspread:1U2aVfh8i-wwy4hV_b18T6AeIwetKSHao_dNPxjwKudc/edit#gid=935903276"))


def test_survey_factory_restores_sources_from_dict():
    survey = SurveyFactory.from_dict({
        'id': 'test-123',
        'creation_date': datetime.isoformat(datetime.now()),
        'sources': [{"type":'test', 'link': 'src1'}, {'type': 'test', 'link': 'src2'}]
    })
    assert_that(survey.sources).contains(SourceURI('test:src1'), SourceURI('test:src2'))


def assert_that_survey_is_basically_valid(survey):
    assert_that(survey.creation_date).is_not_none()
    assert_that(survey.id).is_not_none()


def test_evaluating_a_survey_calculates_mean_for_each_category():
    mock_data_adapter = Mock()
    mock_data_adapter.fetch_from.return_value = pandas.DataFrame({"Timestamp": ["12.03.2020 12:38:51", "12.03.2020 15:58:59", "13.03.2020 19:15:41"],
                                                                  "cat_one": [1, 1, 1],
                                                                  "cat_two": [6, 7, 2]})
    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(datetime.now()), "sources": [{"type": "test", "link": "somelink"}]})

    evaluation = survey.evaluate(data_adapter=mock_data_adapter)

    assert_that(evaluation.categories()['cat_one']['mean']).is_equal_to(1.0)
    assert_that(evaluation.categories()['cat_two']['mean']).is_equal_to(5.0)


def test_evalutin_a_survey_calculcates_median_per_category():
    mock_data_adapter = Mock()
    mock_data_adapter.fetch_from.return_value = pandas.DataFrame(
        {"Timestamp": ["12.03.2020 12:38:51", "12.03.2020 15:58:59", "13.03.2020 19:15:41"],
         "cat_one": [1, 1, 1],
         "cat_two": [6, 7, 2]})

    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(datetime.now()), "sources": [{"type": "test", "link": "somelink"}]})

    evaluation = survey.evaluate(data_adapter=mock_data_adapter)

    assert_that(evaluation.categories()['cat_one']['median']).is_equal_to(1)
    assert_that(evaluation.categories()['cat_two']['median']).is_equal_to(6)


def test_evaluating_a_survey_calculates_health_level_per_category():
    mock_data_adapter = Mock()
    mock_data_adapter.fetch_from.return_value = pandas.DataFrame(
        {"Timestamp": ["12.03.2020 12:38:51", "12.03.2020 15:58:59", "13.03.2020 19:15:41", "13.03.2020 19:16:41"],
         "cat_one": [1, 1, 1, 1],
         "cat_two": [8, 7, 2, 7],
         "cat_three": [9, 10, 10, 9]})

    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(datetime.now()),
                                      "sources": [{"type": "test", "link": "somelink"}]})

    evaluation = survey.evaluate(data_adapter=mock_data_adapter)

    assert_that(evaluation.categories()['cat_one']['healthlevel']).is_equal_to(-100)
    assert_that(evaluation.categories()['cat_two']['healthlevel']).is_equal_to(-25)
    assert_that(evaluation.categories()['cat_three']['healthlevel']).is_equal_to(100)


def test_evaluating_a_survey_calculates_general_health_level():
    mock_data_adapter = Mock()
    mock_data_adapter.fetch_from.return_value = pandas.DataFrame(
        {"Timestamp": ["12.03.2020 12:38:51", "12.03.2020 15:58:59", "13.03.2020 19:15:41"],
         "cat_one": [7, 10, 8],
         "cat_two": [8, 10, 2]})

    survey = SurveyFactory.from_dict({"id": "test-id", "creation_date": datetime.isoformat(datetime.now()),
                                      "sources": [{"type": "test", "link": "somelink"}]})

    evaluation = survey.evaluate(data_adapter=mock_data_adapter)

    assert_that(evaluation.health_level).is_equal_to(17)


def test_survey_evaluation_has_surveyId_and_identifier():
    mock_data_adapter = Mock()
    mock_data_adapter.fetch_from.return_value = pandas.DataFrame(
        {"Timestamp": ["12.03.2020 12:38:51", "12.03.2020 15:58:59", "13.03.2020 19:15:41"],
         "cat_one": [7, 10, 8],
         "cat_two": [8, 10, 2]})

    survey = SurveyFactory.from_dict({"id": "test-id", "identifier": "foo", "creation_date": datetime.isoformat(datetime.now()),
                                      "sources": [{"type": "test", "link": "somelink"}]})

    evaluation = survey.evaluate(data_adapter=mock_data_adapter)

    assert_that(evaluation.surveyId).is_equal_to("test-id")
    assert_that(evaluation.surveyIdentifier).is_equal_to("foo")
