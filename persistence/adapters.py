import abc
import os
from typing import List
import pandas
from domain.survey_vobj import SourceURI


class StorageAdapter(abc.ABC):
    @abc.abstractmethod
    def write_record(self, domain, record_id, content):
        pass

    @abc.abstractmethod
    def get_record_ids_for(self, domain):
        pass

    @abc.abstractmethod
    def get_record(self, domain, id):
        pass


class FileSystemStorage(StorageAdapter):
    def __init__(self, root=''):
        self._root = root

    def write_record(self, domain, record_id, content):
        self.create_domain_folder_if_not_exist(domain)

        with open(self.file_for_record(domain, record_id), "w") as out_file:
            out_file.write(content)

    def create_domain_folder_if_not_exist(self, domain):
        if not os.path.exists(os.path.join(self._root, domain)):
            os.mkdir(os.path.join(self._root, domain))

    def file_for_record(self, domain, record_id):
        return os.path.join(self._root, domain, record_id)

    def get_record_ids_for(self, domain):
        ids = []
        for filename in os.listdir(os.path.join(self._root, domain)):
            ids.append(os.path.splitext(filename)[0])
        return ids

    def get_record(self, domain, record_id):
        with open(self.file_for_record(domain, record_id)) as record:
            return record.read()


class SurveyDataSourceAdapter(abc.ABC):
    @abc.abstractmethod
    def fetch_from(self, source: List[SourceURI]) -> pandas.DataFrame:
        pass


class GSpreadSurveyDataSource(SurveyDataSourceAdapter):
    def fetch_from(self, source: List[SourceURI]) -> pandas.DataFrame:
        return pandas.DataFrame({"Timestamp": ["12.03.2020 12:38:51", "12.03.2020 15:58:59", "13.03.2020 19:15:41"],
                                                                  "cat_one": [1, 1, 1],
                                                                  "cat_two": [6, 7, 2]})


class CsvSurveyDataSource(SurveyDataSourceAdapter):
    def fetch_from(self, source: List[SourceURI]) -> pandas.DataFrame:
        df_list = []
        for src in source:
            df_list.append(pandas.read_csv("data/csv/{}".format(src.link)))

        frame = pandas.concat(df_list, axis=0, ignore_index=True)
        return frame
