import rest
from persistence.adapters import FileSystemStorage
from persistence.survey import FlatfileSurveyRepository
from persistence.team import FlatfileTeamRepository


def main():
    storage_adapter = FileSystemStorage('./data/')
    survey_repository = FlatfileSurveyRepository(storage_adapter)
    team_repository = FlatfileTeamRepository(storage_adapter)

    rest.start_api(survey_repository, team_repository)

if __name__ == '__main__':
    main()
