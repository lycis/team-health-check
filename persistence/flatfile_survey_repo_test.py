import json
from datetime import datetime
from unittest.mock import Mock
from assertpy import assert_that

from domain.survey import SurveyFactory
from domain.survey_vobj import SourceURI
from persistence.survey import FlatfileSurveyRepository


def test_newly_created_survey_has_id_and_is_stored():
    fs_mock = Mock()
    repo = FlatfileSurveyRepository(fs_mock)

    survey = repo.new_survey()
    assert_that(survey.id).is_not_none()
    fs_mock.write_record.assert_called_once_with("survey", survey.id, json.dumps({
        "id": survey.id,
        "creation_date": datetime.isoformat(survey.creation_date)
    }))


def test_can_list_all_existing_survey_ids():
    fs_mock = Mock()
    repo = FlatfileSurveyRepository(fs_mock)
    fs_mock.get_record_ids_for.return_value = ["id-a", "id-b", "id-c"]

    surveys = repo.get_all_survey_ids()

    fs_mock.get_record_ids_for.assert_called_once_with("survey")
    assert_that(surveys).contains("id-a", "id-b", "id-c").is_length(3)


def test_repository_returns_survey_by_id_if_exists():
    fs_mock = Mock()
    repo = FlatfileSurveyRepository(fs_mock)
    fs_mock.get_record.return_value = '{"id": "test-id", "creation_date": "2019-12-04 22:21:54.963413"}'

    survey = repo.get_survey_by_id("test-id")

    assert_that(survey.id).is_equal_to("test-id")
    assert_that(survey.creation_date).is_equal_to(datetime.fromisoformat("2019-12-04 22:21:54.963413"))


def test_repository_saves_identifier_of_survey():
    fs_mock = Mock()
    repo = FlatfileSurveyRepository(fs_mock)
    survey = SurveyFactory.with_identifier("test-123")

    repo.save_survey(survey)

    fs_mock.write_record.assert_called_once_with("survey", survey.id, json.dumps({
        "id": survey.id,
        "creation_date": datetime.isoformat(survey.creation_date),
        "identifier": survey.identifier
    }))


def test_repository_saves_sources_of_surves():
    fs_mock = Mock()
    repo = FlatfileSurveyRepository(fs_mock)
    survey = SurveyFactory.with_identifier("test-123")
    survey.add_source(SourceURI("src:test"))

    repo.save_survey(survey)

    fs_mock.write_record.assert_called_once_with("survey", survey.id, json.dumps({
        "id": survey.id,
        "creation_date": datetime.isoformat(survey.creation_date),
        "identifier": survey.identifier,
        "sources": [{"type": "src", "link": "test"}]
    }))
