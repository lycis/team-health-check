import { $configuration } from '../../config.js'

const Evaluation = {
    template: {{ template('view/evaluation.html') }},
    data () {
        return {
            evaluations: [],
            error: false,
            errorMessage: undefined
        }
    },
    mounted () {
        this.fetchEvaluations()
    },
    methods: {
        fetchEvaluations: function () {
            let ids =  this.$route.params.surveyId
            console.log("fetch " + ids)
            axios.get($configuration.backend_url + "survey/evaluation?survey_ids=" + ids).then(response => {
                this.evaluations = response.data
            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })
        }
    }
}

export { Evaluation }