from assertpy import assert_that

from domain.team import TeamFactory


def test_team_gets_id_on_creation_and_requires_a_name():
    team = TeamFactory.with_name("Test Team")

    assert_that(team.name).is_equal_to("Test Team")
    assert_that(team.id).is_not_none()


def test_can_assign_surveys_to_a_team():
    team = TeamFactory.with_name("Test Team")
    team.assign_survey("survey-id-1")
    team.assign_survey("survey-id-2")

    assert_that(team.assigned_surveys).contains("survey-id-1", "survey-id-2")


def test_assigning_same_survey_twice_only_adds_it_once():
    team = TeamFactory.with_name("Test Team")
    team.assign_survey("survey-id-1")
    team.assign_survey("survey-id-1")

    assert_that(team.assigned_surveys).contains("survey-id-1").is_length(1)
