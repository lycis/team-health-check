import { $configuration } from '../../config.js'

const Surveys = {
    template: {{ template('view/surveys.html') }},
    data () {
        return {
            surveys: [],
            error: false,
            errorMessage: "",
            loading: true,
            identifier: undefined,
            identifierInvalid: false,
            sourceAddTypeData: {},
            sourceAddUriData: {},
            selectedSurveys: []
        }
    },
    mounted () {
        this.loadSurveys()
    },
    methods: {
        evaluateSurveys: function () {
            window.open("http://localhost:5000/app/index.html#/evaluation/" + this.selectedSurveys, "_blank")
        },
        selectSurvey: function (surveyId) {
            if(this.selectedSurveys.includes(surveyId)) {
                this.selectedSurveys.filter(function(value, index, arr){ return value !== surveyId })
            } else {
                this.selectedSurveys.push(surveyId)
            }
        },
        createSurvey: function () {
            if (this.identifier === undefined || this.identifier.length === 0) {
                this.identifierInvalid = true
                $('#survey-identifier-container').transition('shake');
                return
            }

            console.log(this.identifierInvalid)

            let data = JSON.stringify({
                identifier: this.identifier
            })

            axios.put($configuration.backend_url + "survey", data, {
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(response => {
                this.loadSurveys()
                this.identifier = ""
            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })
        },
        resetInvalidIdentifier: function () {
            this.identifierInvalid = false  
        },
        loadSurveys: function () {
            this.loading = true
            this.surveys = []
            axios.get($configuration.backend_url + "survey")
            .then(response => {
                response.data.forEach(id => {
                    axios.get($configuration.backend_url + "survey/" + id)
                        .then(response => {
                            this.surveys.push(response.data)
                        })
                })

                this.loading = false
            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })
        },
        addDataSource: function (survey) {
            let uri = this.sourceAddTypeData[survey.id] + ":" + this.sourceAddUriData[survey.id]
            axios.patch($configuration.backend_url + "survey/" + survey.id,
                        {source: uri},
                        {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                .then(response => {
                    this.loadSurveys()
                }).catch(e => {
                    this.error = true
                    this.errorMessage = e.response.data
                    console.log("Error: " + e.response.data)
                })
        }
    }
}

export { Surveys }