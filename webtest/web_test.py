import uuid

import pytest
from assertpy import assert_that
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as expect


@pytest.fixture(autouse=True)
def navigate_to_site(selenium):
    selenium.get('http://localhost:5000')
    yield


@pytest.fixture(autouse=True)
def firefox_options(firefox_options, pytestconfig):
    firefox_options.headless = True
    return firefox_options


def test_index_is_loaded(selenium: WebDriver):
    assert_that(selenium.find_element_by_id("main-layout")).is_not_none()


def test_navigating_to_surveys_displays_list_of_surveys(selenium: WebDriver):
    navigate_to_survey_view(selenium)


def navigate_to_survey_view(selenium):
    survey_link = selenium.find_element_by_id("navigation-survey")
    assert_that(survey_link).is_not_none()
    survey_link.click()
    assert_that(selenium.find_element_by_id("survey-view")).is_not_none()


def test_adding_a_new_survey_displays_it_in_overview(selenium: WebDriver):
    navigate_to_survey_view(selenium)

    rows_before = len(selenium.find_elements_by_xpath("//table[@id='survey-table']/tbody/tr"))

    identifier = str(uuid.uuid4())
    identifier_input = selenium.find_element_by_id("survey-identifier")
    assert_that(identifier_input).is_not_none()
    identifier_input.click()
    identifier_input.send_keys(identifier)

    add_button = selenium.find_element_by_id("survey-add")
    assert_that(add_button).is_not_none()
    add_button.click()

    Wait(selenium, 10).until(expect.presence_of_element_located((By.ID, 'survey-table')))
    rows_after = len(selenium.find_elements_by_xpath("//table[@id='survey-table']/tbody/tr"))

    assert_that(rows_before).is_less_than(rows_after)
    assert_that(selenium.find_element_by_xpath("//*[text()='{}']".format(identifier))).is_not_none()


def test_must_give_identifier_when_adding_new_survey(selenium: WebDriver):
    navigate_to_survey_view(selenium)

    add_button = selenium.find_element_by_id("survey-add")
    assert_that(add_button).is_not_none()
    add_button.click()

    identifier_input = selenium.find_element_by_id("survey-identifier-container")
    assert_that(identifier_input).is_not_none()

    assert_that(identifier_input.get_attribute("class")).contains("error")


def test_add_source_to_survey(selenium: WebDriver):
    navigate_to_survey_view(selenium)

    identifier = str(uuid.uuid4())
    identifier_input = selenium.find_element_by_id("survey-identifier")
    assert_that(identifier_input).is_not_none()
    identifier_input.click()
    identifier_input.send_keys(identifier)

    add_button = selenium.find_element_by_id("survey-add")
    assert_that(add_button).is_not_none()
    add_button.click()

    add_source_button = selenium.find_element_by_id("add-source-{}".format(identifier))
    assert_that(add_button).is_not_none()
    add_source_button.click()

    Wait(selenium, 10).until(expect.presence_of_element_located((By.ID, 'survey-source-{}-type-selector'.format(identifier))))
    source_type_selector = Select(selenium.find_element_by_id('survey-source-{}-type-selector'.format(identifier)))
    source_type_selector.select_by_value("gspread")
    source_uri_input = selenium.find_element_by_id('survey-source-{}-uri-input'.format(identifier))
    source_uri_input.send_keys('https://example.com/gspread/test/{}'.format(identifier))
    save_button = selenium.find_element_by_id('survey-source-{}-save'.format(identifier))
    save_button.click()

    Wait(selenium, 10).until(expect.presence_of_element_located((By.ID, 'survey-table')))
    selenium.find_element_by_xpath("//*[text()='https://example.com/gspread/test/{}']".format(identifier))
