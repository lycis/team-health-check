class SourceURI:
    def __init__(self, uri: str):
        self._uri = uri

    def __eq__(self, other):
        return other._uri == self._uri

    def _get_type(self):
        return self._uri[0:self._uri.index(':')]

    def _get_link(self):
        return self._uri[self._uri.index(':')+1:]

    type = property(_get_type, None)
    link = property(_get_link, None)