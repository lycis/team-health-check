import json
from json import JSONEncoder
from typing import List

from domain.team import TeamRepository, TeamFactory, Team
from persistence.adapters import FileSystemStorage


class TeamJsonEncoder(JSONEncoder):
    def default(self, o: Team):
        if not isinstance(o, Team):
            return super(TeamJsonEncoder, self).default(o)

        return {
            "id": o.id,
            "name": o.name,
            "assigned_surveys": o.assigned_surveys
        }


class FlatfileTeamRepository(TeamRepository):
    def get_team_by_id(self, team_id) -> Team:
        record_content = self._fs_adapter.get_record("team", team_id)
        data = json.loads(record_content)
        return TeamFactory.from_dict(data)

    def get_all_team_ids(self) -> List[str]:
        return self._fs_adapter.get_record_ids_for("team")

    def __init__(self, fs_adapter: FileSystemStorage):
        self._fs_adapter = fs_adapter

    def new_team(self, name: str) -> Team:
        team = TeamFactory.with_name(name)
        self.save_team(team)
        return team

    def save_team(self, team) -> None:
        self._fs_adapter.write_record("team", team.id, json.dumps(team, cls=TeamJsonEncoder))

