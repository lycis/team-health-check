import { $configuration } from '../../config.js'

const Teams = {
    template: {{ template('view/team.html') }},
    data () {
        return {
            newTeamName: "",
            error: false,
            errorMessage: "",
            teams: [],
            surveyToBeAdded: "",
            surveys: []
        }
    },
    mounted () {
        this.loadTeamData()
        this.loadSurveys()
    },
    methods: {
        addSurveyTo: function (teamId) {
            console.log("add survey '" + this.surveyToBeAdded + "' to '" + teamId + "'")
            axios.patch($configuration.backend_url + "team/" + teamId,
                {assigned_surveys: [this.surveyToBeAdded]}, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(response => {
                this.loadTeamData()
                this.surveyToBeAdded = ""
            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })
        },
        displayModal: function (id) {
            console.log("foo")
            $('#' + id).modal('show');
        },
        createTeam: function () {
            if (this.newTeamName === undefined || this.newTeamName.length === 0) {
                $('#new-team-name').transition('shake');
                return
            }

            let data = { name: this.newTeamName }

            axios.put($configuration.backend_url + "team", data, {
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(response => {
                this.loadTeamData()
                this.newTeamName = ""
            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })            
        },
        loadTeamData: function () {
            this.teams = []
            axios.get($configuration.backend_url + "team").then(response => {
                response.data.forEach(id => {
                    axios.get($configuration.backend_url + "team/" + id)
                         .then(response => this.teams.push(response.data))
                         .catch(e => console.log(e))
                })

            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })
        },
        loadSurveys: function () {
            this.surveys = []
            axios.get($configuration.backend_url + "survey")
            .then(response => {
                response.data.forEach(id => {
                    axios.get($configuration.backend_url + "survey/" + id)
                        .then(response => {
                            this.surveys.push({id: response.data.id, identifier: response.data.identifier})
                        })
                })
            }).catch(e => {
                this.error = true
                this.errorMessage = e.response.data
                console.log("Error: " + e.response.data)
            })
        }
    }
}

export { Teams }