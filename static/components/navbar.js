const Navbar = {
    template: {{ template('component/navbar.html') }},
    computed: {
        isSurveyPage: function() {
            return this.$route.fullPath === "/surveys"
        },
        isAboutPage: function() {
            return this.$route.fullPath === "/about"
        },
        isTeamsPage: function() {
            return this.$route.fullPath === "/teams"
        }
    }
}

export { Navbar }