import json
import mimetypes
from flask import Flask, redirect, jsonify, request, render_template, make_response
from werkzeug.exceptions import abort
from domain.survey import SurveyRepository
from domain.survey_vobj import SourceURI
from domain.team import TeamRepository
from persistence.adapters import GSpreadSurveyDataSource, CsvSurveyDataSource
from persistence.survey import SurveyJsonEncoder, EvaluationResultJsonEncoder
from persistence.team import TeamJsonEncoder

_app = Flask(__name__, template_folder='static')
_app.config['TEMPLATES_AUTO_RELOAD'] = True

_survey_repo = None  # type: SurveyRepository
_team_repo = None # type: TeamRepository


def include_template(name: str):
    template_path = 'templates/{}'.format(name)
    with open(template_path) as template_file:
        content = template_file.read()
    return '`{}`'.format(content)


@_app.context_processor
def inject_jinja_functions():
    return dict(template=include_template)


@_app.route('/app/<path:path>')
def frontend(path: str):
    if len(path) <= 0:
        path = 'index.html'

    response = make_response(render_template(path))
    response.headers['Content-Type'] = mimetypes.guess_type(path)[0]

    return response


@_app.route('/')
def index():
    return redirect('/app/index.html')


@_app.route('/survey', methods=['PUT'])
def create_new_survey():
    survey = _survey_repo.new_survey()

    if request.data:
        if "identifier" in request.json:
            survey.identifier = request.json["identifier"]

    _survey_repo.save_survey(survey)

    return jsonify({"id": survey.id})


@_app.route('/team', methods=['PUT'])
def put_new_team():
    if request.data is None:
        abort(400)

    if "name" not in request.json:
        abort(400)

    team = _team_repo.new_team(request.json["name"])

    return jsonify({"id": team.id})


@_app.route('/survey/<survey_id>', methods=['GET'])
def get_survey_by_id(survey_id):
    survey = _survey_repo.get_survey_by_id(survey_id)
    if survey is None:
        abort(404)

    return json.dumps(survey, cls=SurveyJsonEncoder)


@_app.route('/survey', methods=['GET'])
def get_all_surveys():
    survey_ids = _survey_repo.get_all_survey_ids()
    _app.logger.info("retrieved surveys: {}".format(survey_ids))
    return jsonify(survey_ids)


@_app.route('/team', methods=['GET'])
def get_all_teams():
    team_ids = _team_repo.get_all_team_ids()
    return jsonify(team_ids)


@_app.route('/team/<team_id>', methods=['GET'])
def get_team_by_id(team_id):
    team = _team_repo.get_team_by_id(team_id)
    if team is None:
        abort(404)

    return json.dumps(team, cls=TeamJsonEncoder)

@_app.route('/survey/evaluation', methods=['GET'])
def get_survey_evaluation():
    id_string = request.args.get('survey_ids')
    if id_string is None:
        abort(400)

    ids = id_string.split(",")

    evaluations = []
    for sid in ids:
        survey = _survey_repo.get_survey_by_id(sid)
        if survey is None:
            continue

        evaluation = survey.evaluate(CsvSurveyDataSource())
        evaluations.append(evaluation)

    return json.dumps(evaluations, cls=EvaluationResultJsonEncoder)


@_app.route('/survey/<survey_id>', methods=['PATCH'])
def patch_survey_by_id(survey_id):
    if survey_id is None:
        abort(400)

    if request.data is None:
        abort(400)

    survey = _survey_repo.get_survey_by_id(survey_id)
    if survey is None:
        abort(404)

    if "source" in request.json:
        survey.add_source(SourceURI(request.json["source"]))

    _survey_repo.save_survey(survey)

    return json.dumps(survey, cls=SurveyJsonEncoder)

@_app.route('/team/<team_id>', methods=['PATCH'])
def patch_team_by_id(team_id):
    if team_id is None:
        abort(400)

    if request.data is None:
        abort(400)
    
    team = _team_repo.get_team_by_id(team_id)
    if team is None:
        abort(404)

    if "assigned_surveys" in request.json:
        for survey_id in request.json["assigned_surveys"]:
            survey = _survey_repo.get_survey_by_id(survey_id)
            if survey is None:
                abort(400)
            
            team.assign_survey(survey_id)

    _team_repo.save_team(team)
    return json.dumps(team, cls=TeamJsonEncoder)
            


def start_api(survey_repository: SurveyRepository, team_repository: TeamRepository):
    global _survey_repo
    global _app
    global _team_repo

    _survey_repo = survey_repository
    _team_repo = team_repository
    _app.run()
