import datetime
import json
from json import JSONEncoder

from domain.survey import SurveyRepository, SurveyFactory, Survey, EvaluationResult
from persistence.adapters import FileSystemStorage


class EvaluationResultJsonEncoder(JSONEncoder):
    def default(self, o: EvaluationResult):
        if isinstance(o, EvaluationResult):
            return {
                "categories": o.categories(),
                "health_level": o.health_level,
                "surveyIdentifier": o.surveyIdentifier,
                "surveyId": o.surveyId
            }
        return super().default(0)


class SurveyJsonEncoder(JSONEncoder):
    def default(self, o: Survey):
        if not isinstance(o, Survey):
            return super().default(o)

        serialised = {
            'id': o.id,
            'creation_date': datetime.datetime.isoformat(o.creation_date)
        }

        if o.identifier is not None:
            serialised['identifier'] = o.identifier

        if len(o.sources) > 0:
            src = []
            for source in o.sources:
                src.append({"type": source.type, "link": source.link})
            serialised['sources'] = src

        return serialised


class FlatfileSurveyRepository(SurveyRepository):
    def __init__(self, filesystem_adapter: FileSystemStorage):
        self._filesystem = filesystem_adapter

    def new_survey(self) -> Survey:
        survey = SurveyFactory.new_survey()
        self.save_survey(survey)
        return survey

    def get_all_survey_ids(self):
        return self._filesystem.get_record_ids_for("survey")

    def get_survey_by_id(self, survey_id: str) -> Survey:
        record_content = self._filesystem.get_record("survey", survey_id)
        data = json.loads(record_content)
        return SurveyFactory.from_dict(data)

    def save_survey(self, survey: Survey):
        self._filesystem.write_record("survey", survey.id, json.dumps(survey, cls=SurveyJsonEncoder))

