import abc
import datetime
from uuid import uuid4

import numpy
import pandas

from domain.survey_vobj import SourceURI
from persistence.adapters import SurveyDataSourceAdapter


class IllegalTeamNameError(Exception):
    pass


class IllegalSurveyIdentifier(Exception):
    pass


class EvaluationResult(object):
    def __init__(self, surveyId, surveyIdentifier):
        self._categories = {}
        self._health_level = 0
        self._surveyId = surveyId
        self._surveyIdentifier = surveyIdentifier

    def add_category(self, category, mean, median, healthlevel):
        self._categories[category] = {
            "mean": mean,
            "median": median,
            "healthlevel": healthlevel
        }

    def categories(self):
        return self._categories

    def _set_health_level(self, hl: float):
        self._health_level = hl

    def _get_health_level(self) -> float:
        return self._health_level

    def _get_survey_id(self) -> str:
        return self._surveyId

    def _get_survey_identifier(self) -> str:
        return self._surveyIdentifier

    health_level = property(_get_health_level, _set_health_level)
    surveyId = property(_get_survey_id, None)
    surveyIdentifier = property(_get_survey_identifier, None)


class Survey:
    def __init__(self):
        self._entityId = None
        self._associated_team = None
        self._creation_date = None
        self._identifier = None
        self._sources = []

    def _get_associated_team(self):
        return self._associated_team

    def _set_associated_team(self, name: str):
        if not name.strip():
            raise IllegalTeamNameError()

        self._associated_team = name

    def _set_id(self, entity_id: str):
        self._entityId = entity_id

    def _get_id(self):
        return self._entityId

    def _get_creation_date(self):
        return self._creation_date

    def _set_creation_date(self, date: datetime.datetime):
        self._creation_date = date

    def _get_identifier(self):
        return self._identifier

    def _set_identifier(self, identifier: str):
        if identifier.isspace() or " " in identifier:
            raise IllegalSurveyIdentifier()
        self._identifier = identifier

    def _get_sources(self):
        return self._sources

    associated_team = property(_get_associated_team, _set_associated_team)
    id = property(_get_id, _set_id)
    creation_date = property(_get_creation_date, _set_creation_date)
    identifier = property(_get_identifier, _set_identifier)
    sources = property(_get_sources, None)

    def add_source(self, uri: SourceURI):
        self._sources.append(uri)

    def evaluate(self, data_adapter: SurveyDataSourceAdapter):
        if data_adapter is None:
            return

        data = data_adapter.fetch_from(self.sources)

        evaluation = EvaluationResult(self.id, self.identifier)
        all_values = pandas.Series()
        for i in range(1, len(data.columns)):
            if data.dtypes[i] == 'int64':
                column = data.columns[i]
                values = data[column]
                nps = self.calculate_nps(values)
                evaluation.add_category(column,
                                        mean=values.mean(),
                                        median=values.median(),
                                        healthlevel=nps)
                all_values = pandas.concat([all_values, values])

        evaluation.health_level = self.calculate_nps(all_values)
        return evaluation

    @staticmethod
    def calculate_nps(values) -> int:
        detractors = values[values < 7].count()
        attractors = values[values > 8].count()
        nps = (attractors / values.count()) * 100 - (detractors / values.count()) * 100
        return numpy.around(nps, 0)


class SurveyRepository(abc.ABC):
    @abc.abstractmethod
    def new_survey(self) -> Survey:
        pass

    @abc.abstractmethod
    def get_all_survey_ids(self) -> [str]:
        pass

    @abc.abstractmethod
    def get_survey_by_id(self, suvery_id: str) -> Survey:
        pass

    @abc.abstractmethod
    def save_survey(self, survey: Survey):
        pass


class SurveyFactory:
    @staticmethod
    def new_survey():
        survey = Survey()
        survey.creation_date = datetime.datetime.now(datetime.timezone.utc)
        survey.id = str(uuid4())
        return survey

    @staticmethod
    def from_dict(data: dict) -> Survey:
        survey = Survey()
        survey.id = data["id"]
        survey.creation_date = datetime.datetime.fromisoformat(data["creation_date"])

        if "identifier" in data:
            survey.identifier = data["identifier"]

        if "sources" in data:
            for source in data['sources']:
                survey.add_source(SourceURI("{t}:{l}".format(t=source['type'], l=source['link'])))

        return survey

    @staticmethod
    def with_identifier(identifier: str):
        survey = SurveyFactory.new_survey()
        survey.identifier = identifier
        return survey

