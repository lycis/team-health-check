import abc
from typing import List
from uuid import uuid4


class Team(object):
    def __init__(self, id: str, name: str):
        self._id = id
        self._name = name
        self._surveys = []

    def _get_id(self):
        return self._id

    def _get_name(self):
        return self._name

    def _get_assigned_surveys(self):
        return self._surveys

    id = property(_get_id, None)
    name = property(_get_name, None)
    assigned_surveys = property(_get_assigned_surveys, None)

    def assign_survey(self, survey_id: str):
        if survey_id in self.assigned_surveys:
            return
        self._surveys.append(survey_id)


class TeamFactory(object):
    @staticmethod
    def with_name(name: str) -> Team:
        team = Team(str(uuid4()), name)
        return team

    @classmethod
    def from_dict(cls, data) -> Team:
        team = Team(data["id"], data["name"])

        if "assigned_surveys" in data:
            for survey_id in data["assigned_surveys"]:
                team.assign_survey(survey_id)
        return team


class TeamRepository(abc.ABC):
    @abc.abstractmethod
    def new_team(self, name: str) -> Team:
        pass

    @abc.abstractmethod
    def get_all_team_ids(self) -> List[str]:
        pass

    @abc.abstractmethod
    def get_team_by_id(self, team_id) -> Team:
        pass

    @abc.abstractmethod
    def save_team(self, team: Team) -> None:
        pass
